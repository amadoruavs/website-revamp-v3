document.addEventListener('DOMContentLoaded', function() {
  feather.replace();

  document.querySelector('form').addEventListener('onsubmit', function() {
      document.querySelector('#submit-btn').classList.add("is-loading");     
  });

  p1 = document.querySelector('#page-1');
  softwarep2 = document.querySelector('#software-p2');
  mechp2 = document.querySelector('#mech-p2');
  businessp2 = document.querySelector('#business-p2');

  hide(softwarep2);
  hide(mechp2);
  hide(businessp2);
});

function hide(fieldset) {
  fieldset.style.display = "none";
}

function show(fieldset) {
  fieldset.style.disply="block";
}