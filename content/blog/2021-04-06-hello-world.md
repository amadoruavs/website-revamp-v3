---
title: Hello, World!
date: 2021-04-06T05:33:45.069Z
description: Welcome to our blog!
---
Hey all! We've finally found a good solution to our blog: Netlify CMS. We've wanted to migrate our blog from Medium to our own site for some time, and Netlify CMS has given us a chance to do so, while integrating it into our new Hugo-built static site. Stay tuned for many articles to come!