---
title: "The Team"
date: 2021-05-04T23:51:50Z
draft: false
type: "pages"
layout: "the-team"

hero_title: The Team
hero_tagline: Our team is made up of many brilliant high school engineers.
hero_image: /who-are-we.webp

team_sections:
    - name: Software Team
      description: Our software team handles all the software systems on the drone from end to end. Whether it's writing and improving path planning routines for our drone, building aerial computer vision systems, or contributing needed open-source features to the PX4 autopilot,the Software division is constantly working on new things.
      image: /software-picture1.webp
      members:	
        - image: /team_members/kush_nayak.webp
          name: Kush Nayak
          role: Software Lead
        - image: /team_members/raghav_misra.webp
          name: Raghav Misra
          role: Image Processing 
        - image: /team_members/angad_bhargav.webp
          name: Angad Bhargav
          role: Website Development
        - image: /team_members/nikhil_sunkad.webp
          name: Nikhil Sunkad
          role: Object Detection
        - image: /team_members/karan_gupta.webp
          name: Karan Gupta
          role: Ground Control Software
        - image: /team_members/lucas_zhong.webp
          name: Lucas Zhong
          role: Path Planning
        - image: /team_members/sahil_mehta.webp
          name: Sahil Mehta
          role: Software
        - image: /team_members/jishnu_deep.webp
          name: Jishnu Deep
          role: Software
    - name: Electromechanical Team
      description: Our electromechanical team handles all things hardware on the drone. Whether it's upgrading motors and ESCs, designing stronger and better parts with CAD, or wiring up electrical components, they do it all.
      image: /mech.webp
      members:
        - image: /team_members/tony_li.webp
          name: Tony Li
          role: Mechanical
        - image: /team_members/aditi_pattanshetti.webp
          name: Aditi Pattanshetti
          role: Electrical Lead
        - image: /team_members/aayush_gupta.webp
          name: Aayush Gupta
          role: Mechanical Lead
        - image: /team_members/dylan_kwong.webp
          name: Dylan Kwong
          role: President
        - image: /team_members/akshay_javvaji.webp
          name: Akshay Javvaji
          role: Mechanical
    - name: Business Team
      description: The business team is constantly thinking of ways to promote our work and secure critical funding. The business division is responsible for contacting companies, managing sponsorships, and organizing publicity for the club.
      image: /business-picture.webp
      members:
        - image: /team_members/olivia_cheng.webp
          name: Olivia Cheng
          role: Business Lead
        - image: /team_members/darsh_shah.webp
          name: Darsh Shah
          role: Digital Growth
        - image: /team_members/shantelle_tupaz.webp
          name: Shantelle Tupaz
          role: Graphic Designer
    - name: Alumni
      description: AmadorUAVs members who have graduated high school.
      image: /early-picture1.webp
      members:
        - image: /team_members/kalyan_sriram.webp
          role: University of Wisconsin Madison
          name: Kalyan Sriram - Founder
        - image: /team_members/kai_gottschalk.webp
          role: Cal Poly SLO
          name: Kai Gottshalk - Founder
        - image: /team_members/derick_mathews.webp
          role: UC Santa Cruz
          name: Derick Mathews - Founder
        - image: /team_members/justin_park.webp
          role: Virginia Tech
          name: Justin Park - Founder
        - image: /team_members/vincent_wang.webp
          role: Purdue University
          name: Vincent Wang - Founder
        - image: /team_members/ishan_duriseti.webp
          role: UC San Diego
          name: Ishan Duriseti - Founder
        - image: /team_members/frank_yu.webp
          role: UC Santa Barbra 
          name: Frank Yu - Founder
        - image: /team_members/marie_lee.webp
          role: San Jose Sate University
          name: Marie Lee - Founder
        - image: /team_members/kathryn_xiong.webp
          role: University of Washington
          name: Kathryn Xiong
        - image: /team_members/justin_yu.webp
          role: UC Berkeley
          name: Justin Zhao
        - image: /team_members/siddharth_bhargav.webp
          role: University of Texas Austin
          name: Siddarth Bhargav
        - image: /team_members/adam_tout.webp
          role: Harvard
          name: Adam Tout
        - image: /team_members/sungje_park.webp
          role: Embry-Riddle Aeronautical University
          name: Sungje Park - Founder
        - image: /team_members/athan_yang.webp
          role: Purdue University
          name: Athan Yang
        - image: /team_members/ethan_apalis.webp
          role: Cal Poly SLO
          name: Ethan Apalis
        - image: /team_members/matthew_zhao.webp
          role: Software Engineer at Fusebit
          name: Matthew Zhao
---
