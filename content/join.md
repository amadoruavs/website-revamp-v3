---
title: "Join Us"
date: 2021-03-16T15:44:00Z
draft: false
type: "pages"
layout: "join"

hero_title: Join Us!
hero_tagline: All AVHS students are welcome to join the AmadorUAVs team year-round. Scroll down for info.
hero_image: /our-journey.png

#entrance_assignment_instructions:
#      title: AmadorUAVs Entrance Assignment Submissions 2022-2023
#      instructions: Hey everyone, thank you for your interest in being part of AmadorUAVs 2021-2022 team. Below are the submission steps; simply choose the division you want to submit an entrance assignment to and follow the form. If you are applying for more than one division, feel free to resubmit as many times as you wish. If you have any questions or concerns, email officers@amadoruavs.com or the officer responsible for each division (on the Entrance Assignment Doc). Our club has rolling admissions so you can apply anytime you would like. However, we have a soft deadline of October 13th so please submit your assignment by then to have the best chance. If you do not complete all the parts of the entrance assignment please still submit your entrance assignment. You still have a great chance of making the team! Good luck and have fun! — AmadorUAVs 2021-2022 Officer Team
#      software_link: https://docs.google.com/document/d/12gpJapBs1EUJFrafhnyvNfLFgWoV4l6mnG6GA35luXw/edit?usp=sharing
#      mechanical_link: https://docs.google.com/document/d/1uOCPPpj6nxFZbsb-9eiGWU-Uyo_C5-gf-SPzXd67l3U/edit?usp=sharing
#      electrical_link: https://docs.google.com/document/d/1BFGGC8VuI7J4pvkmz7DVObx_oLY7edX6LpVUNUjGv50/edit?usp=sharing
#      business_link: https://docs.google.com/document/d/15W_vrN4R0SYRGZEqPISrB5cNp651vIrNSZZZiw6Kppc/edit?usp=sharing

mailing_list_instructions:
      title: AmadorUAVs Mailing List Sign Up
      instructions: Hey everyone, thank you for your interest in being part of AmadorUAVs 2022-2023 team. Please enter your contact information in this form to sign up for our mailing list. We will keep you up to date on training workshops, events, and cool projects our club has! 
---
