---
title: "About Us"
date: 2021-03-16T15:44:00Z
draft: false
type: "pages"
layout: "about-us"

hero_title: About Us
hero_tagline: How we are building the future of autonomous aviation. 
hero_image: /compteam.webp

about_sections:
    - title: Who are we?
      content: AmadorUAVs is a club at Amador Valley High School in Pleasanton, CA. This club was formed in 2018 with the intent of developing Unmanned Aerial Vehicles (UAVs). This year, our team competed in the 2022 international Student Unmanned Aerial systems (SUAS) competition, and we plan to return to the upcoming 2023 SUAS competition. We have been, and continue working hard to prepare our drone for the specific set of tasks the competition involves.
      image: /who-are-we.webp
    - title: Our Beginnings
      content: Starting in 2018, we developed multiple prototype airplanes before switching to multicopters (drones). Since then, we have designed and constructed several versions of our Micron quadcopter and our competition test-bench octocopter, Macron. Recently, we have been working on our fully custom, competition-ready octocopter, Boreas.
      image: /our-journey.webp
    - title: Our Projects
      content: In addition to designing, manufacturing, and assembling custom drones for the SUAS Competition, our team takes part in a variety of outreach and education events. We've been a part of NXP's Hovergames competition, attended the Redefining Mobility Summit, Tri-Valley Innovation Fair, and held educational workshops for middle school students during ACE Code Day.
      image: /fair.jpg
    - title: SUAS 2022 Competition Journey
      content: For the first time, our team had the opportunity to travel to the SUAS Competition, held in Maryland during the summer of 2022. Learn more about our competition experience at Pleasanton Weekly's article on our home page.
      image: /comp2.jpg 

sections:
    - title: Electromechanical Team
      description: Our electromechanical team handles all things hardware on the drone. Whether it's upgrading motors and ESCs, designing stronger and better parts with CAD, or wiring up electrical components, they do it all.
      cards:
        - image: /killerkalyan.png
          name: Kalyan Sriram
          role: Supreme Leader
          projects:
            - name: Competition
              link: "https://www.auvsi-suas.org"
            - name: AVHS Streaming
              link: "https://amadoruavs.com/avfootball/"
        - image: /team_members/vincent_wang.webp
          name: Vincent Wang
          role: Software Lead
          projects:
            - name: Project 1
              link: "#"
            - name: Project 2
              link: "#"
        - image: /killerkalyan.png
          name: Kalyan Sriram
          role: Supreme Leader
          projects:
            - name: Project 1
              link: "#"
            - name: Project 2
              link: "#"
        - image: /team_members/vincent_wang.webp
          name: Vincent Wang
          role: Software Lead
          projects:
            - name: Project 1
              link: "#"
            - name: Project 2
              link: "#"
        - image: /killerkalyan.png
          name: Kalyan Sriram
          role: Supreme Leader
          projects:
            - name: Project 1
              link: "#"
            - name: Project 1
              link: "#"
    - title: Software Team
      description: Our software team handles all the software systems on the drone from end to end. Whether it's writing and improving path planning routines for our drone, building aerial computer vision systems, or contributing needed open-source features to the PX4 autopilot,the Software division is constantly working on new things.
      cards:
        - image: /team_members/vincent_wang.webp
          name: Vincent Wang
          role: Software Lead
        - image: /killerkalyan.png
          name: Kalyan Sriram
          role: Supreme Leader
        - image: /team_members/vincent_wang.webp
          name: Vincent Wang
          role: Software Lead
        - image: /killerkalyan.png
          name: Kalyan Sriram
          role: Supreme Leader
        - image: /team_members/vincent_wang.webp
          name: Vincent Wang
          role: Software Lead
    - title: Business Team
      description: The business team is constantly thinking of ways to promote our work and secure critical funding. The business division is responsible for contacting companies, managing sponsorships, and organizing publicity for the club.
      cards:
        - image: /killerkalyan.png
          name: Kalyan Sriram
          role: Supreme Leader
        - image: /team_members/vincent_wang.webp
          name: Vincent Wang
          role: Software Lead
        - image: /killerkalyan.png
          name: Kalyan Sriram
          role: Supreme Leader
        - image: /team_members/vincent_wang.webp
          name: Vincent Wang
          role: Software Lead
        - image: /killerkalyan.png
          name: Kalyan Sriram
          role: Supreme Leader
---

