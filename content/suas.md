---
title: "SUAS | AmadorUAVs"
date: 2021-03-16T15:44:00Z
draft: true
type: "pages"
layout: "suas"

hero_title: AUVSI SUAS
hero_tagline: An overview of the systems we develop for the annual AUVSI-SUAS competition.
hero_image: https://images.squarespace-cdn.com/content/v1/5d554e14aaa5e300011a4844/1565872741138-KR612R9R8WQYDPVKCI19/ke17ZwdGBToddI8pDm48kFKa9V0z_dJgqIL74K6UHXQUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYwL8IeDg6_3B-BRuF4nNrNcQkVuAT7tdErd0wQFEGFSnDfoT8m7Ed21kWSrU5hSpAcOAnl-iDhIZCdcqDtcBAahqtvi6nu8gl9Y0NUdWhUOAQ/group_photo.jpg?format=2500w

about_sections:
    - title: What is AUVSI SUAS?
      content: AUVSI SUAS is a collegiate-level autonomous drone participation that AmadorUAVs participates in every year. It is one of the primary focuses of our club, and the main project we develop hardware and software for.
      image: /project_pictures/suas.png
      links:
      - text: AUVSI SUAS Website
        href: https://auvsi-suas.org
        variant: is-black
---
