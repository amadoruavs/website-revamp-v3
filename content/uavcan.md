---
title: "About Us | AmadorUAVs"
date: 2021-03-16T15:44:00Z
draft: true
type: "pages"
layout: "uavcan"

hero_title: UAVCAN GCS
hero_tagline: A look at our implementation of the UAVCAN communication protocol.
hero_image: /Macron_Render.jpg

about_sections:
    - title: UAVCAN GCS
      content: Add text here.
      image: /project_pictures/uavcan.png
---
