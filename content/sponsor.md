---
title: "Sponsor Us"
date: 2021-03-16T15:44:00Z
draft: false
type: "pages"
layout: "sponsor"

hero_title: Sponsor Us
hero_tagline: Support our team's development and get perks like having your logo displayed on the drone and more!
hero_image: /compwin.jpg

cool_people_sections:
- title: Platinum ($2000+)
  image: "/plat-cool_people.png"
  content: "Company logo broadcast at SUAS Competition event, medium logo on the competition poster and team apparel, acknowledgement on our team website and at social events, logo on the Sponsorship Packet, priority over social media spotlights"
- title: Gold ($1000+)
  image: /gold-cool_people.png
  content: "Company logo broadcast at SUAS Competition events, medium logo on the competition poster and team apparel, acknowledgement on our team website and at social events, logo on the Sponsorship Packet"
- title: Silver ($500+)
  image: /silver-cool_people.png
  content: "Company logo broadcast at SUAS Competition events, medium logo on the competition poster and team apparel, acknowledgement on our team website, logo on the Sponsorship Packet"
- title: Bronze ($250+)
  image: /bronze-cool_people.png
  content: "Small logo on the competition poster and team apparel, acknowledgement on our team website, logo on the Sponsorship Packet"

big_cool_people:
- image: /cool_people/air_supply.png
  link: https://airsupply.com
- image: /cool_people/the_imaging_source.png
  link: https://theimagingsource.com
- image: /cool_people/skb.png
  link: https://skbcases.com
- image: /cool_people/zubax.png
  link: https://zubax.com
- image: /cool_people/ppie.png
  link: https://ppie.org
- image: /cool_people/datron.png
  link: https://www.datron.com
- image: /cool_people/holybro.png
  link: http://www.holybro.com
- image: /cool_people/xoar.png
  link: https://xoarintl.com

cool_people:
- image: /cool_people/kctool.png
  link: https://www.kctool.com/
- image: /cool_people/west3D.png
  link: https://west3d.com/
- image: /cool_people/mandala.png
  link: https://mandalaroseworks.com
- image: /cool_people/deep_fried_hero.png
  link: https://deepfriedhero.in/
- image: /cool_people/vip_audio_visual_company.png
  link: https://store.cuav.net
- image: /cool_people/cuav.png
  link: https://www.vipav.com
- image: /cool_people/insta360.png
  link: https://www.insta360.com
- image: /cool_people/computar.png
  link: https://computar.com
- image: /cool_people/digikey.png
  link: https://www.digikey.com
- image: /cool_people/fastsigns.png
  link: https://fastsigns.com
- image: /cool_people/jlcpcb.png
  link: https://jlcpcb.com
- image: /cool_people/pololu.png
  link: https://pololu.com
- image: /cool_people/ptsa.png
  link: https://amadorptsa.com
- image: /cool_people/rfdesign.png
  link: https://rfdesign.com.au
- image: /cool_people/sensirion.png
  link: https://sensirion.com
- image: /cool_people/simscale.png
  link: https://simscale.com
- image: /cool_people/startech.png
  link: https://startech.com
- image: /cool_people/weller.png
  link: https://weller-tools.com



affiliates:
- image: /cool_people/px4.png
  link: https://px4.io
- image: /cool_people/uavcan.png
  link: https://uavcan.org
- image: /cool_people/mavsdk.png
  link: https://mavsdk.mavlink.io
- image: /cool_people/mavlink.png
  link: https://mavlink.io
- image: /cool_people/dronecode.png
  link: https://dronecode.org

---
