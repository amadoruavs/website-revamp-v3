---
title: "Streaming"
date: 2021-03-16T15:44:00Z
draft: false
type: "pages"
layout: "streaming"

livestreams:
    - nav_title: "Graduation 2023"
      confetti: True
      header: "WATCH IT LIVE:"
      stream_id: "nTpVrVV4q3Q"
      title: "Graduation of the AVHS Class of 2023"
      scheduled_time: "June 2, 2023 - Coming Soon!"
      description: "AmadorUAVs is proud to present a live broadcast of Amador Valley High School's Class of 2023's Graduation. A sincere thanks to all who played a role in bringing this production to life." 
    - nav_title: "Graduation 2022"
      confetti: True
      header: "WATCH IT LIVE:"
      stream_id: "o9JZA0BQ1nM"
      title: "Graduation of the AVHS Class of 2022"
      scheduled_time: "June 3, 2022 - 7:00 to 10:00 PM"
      description: "AmadorUAVs is proud to present a live broadcast of Amador Valley High School's Class of 2022's Graduation. A sincere thanks to all who played a role in bringing this production to life." 
    - nav_title: "Graduation 2021"
      confetti: True
      header: "COMPLETE CEREMONY:"
      stream_id: "0IkvX1tWGws"
      title: "Graduation of the AVHS Class of 2021"
      scheduled_time: "May 28, 2021 - 7:00 to 10:00 PM"
      description: "AmadorUAVs is proud to present Amador Valley High School's Class of 2021 in the annual graduation ceremony. Thanks to all of the individuals and organizations that have made this production possible. Note: Due to technical difficulties at the beginning of the live program, our team has remastered portions of the video for enhanced vieweing. To those this may have inconvenienced, our deepest apologies."
    - nav_title: "Varsity Football"
      confetti: False
      header: "Previous Stream:"
      stream_id: "CaEpGgRDLN8"
      title: "Varsity Football - Amador vs. Granada"
      scheduled_time: "Mar 27, 2021 - 7:00 to 9:00 PM"
      description: "In our club's first ever stream, the Amador Valley Dons faced the Granada Matadors and won 48 - 14. Unfortunately, due to COVID-19 infections, this was the last home varsity game Amador played in this makeup season."
      include_archive_link: True
    - nav_title: "JV Football"
      confetti: False
      header: "Previous Stream:"
      stream_id: "eamhEscD-Go"
      title: "Junior Varsity Football - Amador vs. Foothill"
      scheduled_time: "Apr 17, 2021 - 2:00 to 4:00 PM"
      description: "In our Junior Varsity team's last game of the season, the Amador Valley Dons faced the Foothill Falcons and, through an intense game, lost  6 - 12."
      include_archive_link: True
    - nav_title: Freshman Football
      confetti: False
      header: "Previous Stream:"
      stream_id: "kscDNfLxWbk"
      title: "Freshman Football - Amador vs. Acalanes"
      scheduled_time: "Apr 10, 2021 - 2:00 to 4:00 PM"
      description: "In our Freshman team's last game of the season, the Amador Valley Dons faced the Alcalanes Dons and won 22 - 20."
      include_archive_link: True

hero_title: Streaming
hero_tagline: A detailed look at our school streaming services.
hero_image: /Macron_Render.jpg

about_sections:
    - title: AUVSI-SUAS Technical Details
      content: We livestream football games and other major events taking place on the campus of Amador Valley High School.
      image: /project_pictures/streaming.png
---
