---
title: "AmadorUAVs"
date: 2021-03-16T15:44:00Z
draft: false

sections:
  
  join-us:
    title: "A good drone team needs great engineers."
    background: "/aboutus-picture.jpg"
    cards:
      - title: "Electromechanical Division"
        description: "Get hands-on experience designing and building robust, versatile autonomous vehicles."
      - title: "Software Division"
        description: "Learn how to interact with and use industry-standard UAV APIs, build advanced path planning and navigation systems, and more."
      - title: "Business Division"
        description: "Gain valuable experience interacting with sponsors and companies to secure funding and build business relationships."
    buttons:
      - text: "Join! (Coming soon)"
        link: "/"
  about-us:
    title: "We're helping create the future of UAVs."
    background: "/streaming.jpg"
    cards:
      - title: "AUVSI SUAS Competition"
        description: "AUVSI's premier collegiate-level autonomous UAV competition. Our team's primary focus."
      - title: "AVHS Drone Streaming"
        description: "Our project to provide real-time sports broadcasts and other event footage from the air."
      - title: "PX4 Contribution"
        description: "Our team is highly active within the PX4 open source autopilot community, making regular contributions to the Dronecode ecosystem."
  sponsorships:
    title: "Our sponsors make this team possible. Find out how you can help us."
    background: "/macron.jpg"
    buttons:
      - text: "Sponsor Information"
        link: "/sponsor/"
---
