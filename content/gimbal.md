---
title: "About Us | AmadorUAVs"
date: 2021-03-16T15:44:00Z
draft: true
type: "pages"
layout: "gimbal"

hero_title: Gimbal System
hero_tagline: An explanation of our custom-made gimbal system.
hero_image: /Macron_Render.jpg

about_sections:
    - title: Gimbal
      content: We use a custom-designed 3-axis brushless gimbal for stabilization, direction, and control of our Imaging Source DFK-33UX183 industrial camera. This camera features a 1-inch sensor capable of 20-megapixel images at 18 FPS. Attached is a Computer V3522-MPZ 35mm fixed zoom machine vision lens. This camera was chosen because while a high FPS is not necessary for aerial survey tasks, the high sensor resolution allows for the capture of high-definition ODLC objects even when flying at high altitudes. This camera system connects to our onboard companion computer via USB 3.0 and interfaces with the GCS for use for the ODLC survey task.
      image: /project_pictures/gimbal.png
---
